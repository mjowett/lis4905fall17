# **LIS4905 Directed Independent Study**

## **Jonathan Sapp**

**Team mate:**
- [Landon Thaler:](../lmt14b/README.md "Landon's repo") lmt14b@my.fsu.edu    


**Projects**  
- [Powerline](powerline/README.md "Powerline README")  
- [Internship](internship/README.md "Internship README")   


**Finished Tasks**       
- Created and tested Powerline application      
- Tested internship application
 