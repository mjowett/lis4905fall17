# **LIS4905 Directed Independent Study**

## **Jonathan Sapp**

**Team mate:**
- [Landon Thaler:](../../lmt14b/README.md "Landon's repo") lmt14b@my.fsu.edu  

**Finished Tasks**  
- Created this README.md file  
- Created Powerline application  

*Screenshots of Powerline application*:  
![Powerline application](img/powerline.PNG)
![Powerline application](img/powerline2.PNG)

**Overview**  
Not conventional so there is a big learning curve but it is very easy to collaborate.

**Benefits**  
	- Allows for easy data entry  
	- Can make edits easily  
	- Makes collaborating easier  

**Issues**  
	- Website is not responsive   
	- Some functionality will be lost if you ever change the browser size  
	- Field type keeps changing to Email Address (from text) after hitting save  
![Error](img/broken.PNG)   
![Error](img/broken2.PNG)