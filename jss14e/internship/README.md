# **LIS4905 Directed Independent Study**

## **Jonathan Sapp**

**Team mate:**
- [Landon Thaler:](../../lmt14b/README.md "Landon's repo") lmt14b@my.fsu.edu  

**Links**  
- [Log in](login/README.md "Log in readme")

**Accomplished**  
- Tested Site Supervisor component    


**Issues**  
- Duplicate records are possible    
- Can't delete records  
- No client side validation when editing records     
- Employer field automatically has King Arthur s Tools selected  
- Client side validation says phone accepts hyphens but it doesn't  
- User can enter in phone number less than  digits  


**Suggestions**  
- Put newer entries at the top of the list  
- Keep employer field empty so user has to select  

