# **LIS4905 Directed Independent Study**

## **Login**

**Team mate:**
- [Landon Thaler:](../../../lmt14b/README.md "Landon's repo") lmt14b@my.fsu.edu  


**Research**  
- Log in page needs to be clear visible  
- People would rather leave site      
- Users admit to entering incomplete data on registration forms  
https://davidwalsh.name/5-features-login-system  

**Plan**  
- Make log in button large so it can't be missed  
- Keep design simple     
- Lock user after several failed login attempts    
- Prevent SQL injection     


**Point of Reference**  
https://bootsnipp.com/snippets/featured/clean-modal-login-form    

* Demo: [Login](http://landonthaler.com/ "Demo")    
  
**Final Questions**    
1. What is session hijacking?    
Session hijacking is a web attack that consists of someone obtains the user's session ID and pretends to be that user. While pretending to be the user, you can do anything that user had access to do.      
2. How can it be prevented?    
Making the webpage secure (HTTPS) and generating a second secure cookie at the loging page could work. Anytime a user went to a page that contained sensative information you could check back to see if the sucure cookie was still there. An SSL (secure sockets layer) would also prevent session hijacking.   
3. What would you do? Why? Why not?    
To prevent this I would get an SSL certificate for my website.  