# **LIS4905 Directed Independent Study**

## **Benjamin Bloodworth - Fall 17:**

**Team mate:**
- [Ariana Davis:](../amd14b/README.md "Ariana's repo") amd14b@my.fsu.edu

**Powerline Web App:**
- [Powerline Web App](https://codesolve-eval.azurewebsites.net/FormTmpl/FormtmplView) 

**Internship Web App:**
- [Internship Web App](internship/README.md) 

**Internship Web App Login Strategy:**

Our initial strategy was a bit ambitious given our time frame. We implemented a subsection of these requirements as indicated.

+ Purchase an SSL certificate and add it to the server. Login and password information should never be sent across the internet unencrypted. An SSL certificate should be purchased to protect everyone's login information.
+ Create a server side redirect rule that redirects an http request for this page to an https request. This way we can enforce a secure connection.
+ UI should include fields for login and password. **(implemented)**
+ UI should include a link to reset a user's password in case they forgot it. **(implemented)**
+ UI should provide validation feedback to the user if they fail to provide a valid username and password. We don't want to specify which part of the combination is invalid because that is additional information a malicious user can use to try and compromise a login. We just need to display a generic message that their login infomration is incorrect. **(implemented)**
+ UI should do client side validation before submitting to the server. If we decide to use email addresses for passwords, then the UI should validate that a valid email address is provided for the login field. Additionally, we need to decide on some rules for a valid password. What is the minimum and maximum length of the password? Are we going to require letters, numbers, and special symbols? The UI should validate that the password meets these rules before submitting to the server. **(validates that the username is at least 6 characters in length and the password is at least 8 characters in length.)**
+ Server side processing script should perform the same username and password validation before making the database request.
+ Service side processing script needs to return an error message to the client in the event that the username or password is invalid. The UI will be responsible for rendering this message. **(implemented)**
+ To prevent brute force attacks, we should display a CAPTCHA challenge after 3 failed logins. We don't want to lock the account out due to failed logins because that creates a way for a malicious user to lock someone out of their account. A CAPTCHA challenge will stop brute force attacks without preventing a legitimate user from accessing their account. By not requiring a CAPTCHA unless the current user has failed three to login successfully three times in a row, we eliminate frustrations for legitimate users.
* It would be nice to integrate the login with the FSU domain server, but it's unclear if this application allows individuals without an FSU account to access the application.
* If we can't integrate with the FSU domain server, then we need to develop a database backend for storing login information. We need to add a salting and encryption algorithim to secure the user's password in the database table. **(database solution implemented)**

[Demo zip file](./login-demo.zip)

**Working Demo**
[http://www.benjaminbloodworth.com/lis4905/login.php](http://www.benjaminbloodworth.com/lis4905/login.php)

**Sessions**
1. What is "Session Hijacking"?
Session hijacking is intercepting an authenticated user's cookie so that a malicious user can impersonate them.
2. How can it be prvented?
There are many methods, the simplest of which is using an SSL/TLS connection. Another method is to use a random number in the cookie that is updated and validated on each request.
3. What would *you* do? Why? Why not?
I would implement SSL/TLS. The additional overhead is in creating the initial secure connection. After that, performance impact is negligible.


**Finished Tasks**
- Created personal subdirectory and README.md file

*Screenshot of PowerLine Web App forms fields*:
![Powerline WebA App](img/powerline_1.png)

#### Issues
* It wasn't very intuitive. I kept wanting more data types to match against.
* It was weird to always choose text data type even for numeric data.

#### Benefits 
* It's a good product for its intended audience, which I believe is non developers.
* It doesn't offer enough control for anyone with database design experience, for example you can't set indexes.

#### Overall Review 
* I think this product does a reasonable job of translating a desktop database to an Azure environment without require experience with Azure.

## License

For more information about the license, see http://example.com/license/

Copyright (c) 2017 **lis4905fall17**

Licensed under the **Git 'R Dun** license.