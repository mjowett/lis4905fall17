# **LIS4905 Directed Independent Study**

## **Benjamin Bloodworth - Internship**

**Team mate:**
- [Ariana Davis:](../amd14b/README.md "Ariana's repo") amd14b@my.fsu.edu

**Internship Web App:**
- [Internship Web App](http://internship.qcitr.com/) 

## Issues
### Home Page
* Page language defaults to German.
### Assignments
* Intern (last name, first name) field: With only one option available, the field never turns green to show that it is valid because the selection doesn't change. I suggest adding a please choose option so that validation highlighting will work.
* Adding the same intern to an employer redirects to an error page. The header for the Error page is black instead of garnet.
* Does not default to most recently entered intern's information.
* Year accepts 9999 but displays 0000. No error message is displayed.
* Inconsistent naming of fields between add screen and edit screen.
* Providing a value of 999 for int_id produces a fatal error: Call to undefined function dispy_db_error() in E:\HostingSpaces\mjowett\internship.qcitr.com\wwwroot\global\functions.php on line 542. Additionally, error messages should never be returned to the client.
* Deleting the last record in the assignments page displays a foreach() error: Invalid argument supplied for foreach() in E:\HostingSpaces\mjowett\internship.qcitr.com\wwwroot\assignment\index.php on line 75.
* http://internship.qcitr.com/assignment/index.php displays a foreach() error when there are no assignments: 
Warning: Invalid argument supplied for foreach() in E:\HostingSpaces\mjowett\internship.qcitr.com\wwwroot\assignment\index.php on line 75. Additionally, error messages should never be returned to the client.
* Pay accepts any character. No error is generated server side.
* Offer accpets any character. No error is generated server side.
* Offer accepted accepts any character. No error is generated server side.

#### Incosistent validation highlighting
* Pay field: It doesn't turn green when valid like the other fields.
* Offer, Offer Accepted and Notes aren't required, but Notes: turns green after entering text while the other fields do not.

#### Suggestions
* All drop down lists should include a blank option to make sure the user actually selected something. This helps with validation.

## License

For more information about the license, see http://example.com/license/

Copyright (c) 2017 **lis4905fall17**

Licensed under the **Git 'R Dun** license.