# LIS4905 Directed Independent Study

## Login Demo Repo

### Team:
Jee Kim

Stephen Keating

- Researching a basic secure login using PHP and MySQL. 

    [Login System Tutorial Link](http://forums.devshed.com/php-faqs-stickies-167/program-basic-secure-login-system-using-php-mysql-891201.html "Login System Tutorial")

        * step by step breakdown of how to implement
        * thoroughly outlines
            * security issues solved (ie- SQL injection, XSS attacks, password hashing and storage)
            * database interactions (identical to the PDO useage in mobile applications class and CRUD capabilities needed to manage user accounts)
            * general PHP topics needed to create application.
            
        
    [Another Secure Login Tutorial Link](https://github.com/DeveloperShed/BasicSecureLogin "Another Secure Login Tutorial")
        
        * source code and updates for needed PHP documents to implement system.

	- Concept demonstration - 
	[Login Demo Link](ec2-54-221-132-245.compute-1.amazonaws.com "Login Demo")

	[Login Demo Codes](./loginpages "Login Demo Codes")
    
    [Login Zip File](./Login.zip "Login Zip File")


### What is "Session Hijacking"?

* Session Hijacking is a method to steal someone else's session ID to gain unauthorized access to a web server

### How can it be prevented?

* Force HTTPS for all pages, not just the login page.
* Make session tokens complex and unpredictable by using a generator
* Make sure the session ends or expires after a certain period of time or when a user logs out

### What would *you* do? Why? Why not?

* Never use internet and live off the grid
* Minimize the use of public WiFi, use a VPN when necessary to encrypt the network packets
* Ensure that websites are using HTTPS when logging in
