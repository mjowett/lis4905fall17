<?php
    require("connection.php");
    if(!empty($_POST))
    {
        if(empty($_POST['username']))
        {
            die("Please enter a username.");
        }
        if(empty($_POST['password']))
        {
            die("Please enter a password.");
        }

        if(empty($_POST['confirmpass']))
        {
            die("Please confirm your password.");
        }

        if($_POST['password'] != $_POST['confirmpass'])
        {
            die("Password mismatch. Please re-enter your password.");
        }

        $query = "
            SELECT
                1
            FROM users
            WHERE
                username = :username
        ";
        
        $query_params = array(
            ':username' => $_POST['username']
        );
        
        try
        {
            $stmt = $db->prepare($query);
            $result = $stmt->execute($query_params);
        }
        catch(PDOException $ex)
        {
            die("Failed to run query");
        }
        $row = $stmt->fetch();
        if($row)
        {
            die("This username is already in use");
        }
        $query = "
            INSERT INTO users (
                username,
                password,
                salt
            ) VALUES (
                :username,
                :password,
                :salt
            )
        ";
        $salt = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647));
        $password = hash('sha256', $_POST['password'] . $salt);
        for($round = 0; $round < 65536; $round++)
        {
            $password = hash('sha256', $password . $salt);
        }
        $query_params = array(
            ':username' => $_POST['username'],
            ':password' => $password,
            ':salt' => $salt
        );
        
        try
        {
            $stmt = $db->prepare($query);
            $result = $stmt->execute($query_params);
        }
        catch(PDOException $ex)
        {
            die("Failed to run query");
        }
        header("Location: index.php");
        die("Redirecting to index.php");
    }
?>