 <?php
    require("connection.php");
    if(empty($_SESSION['user']))
    {
        header("Location: index.php");
        die("Redirecting to index.php");
    }
?>
<h1>Welcome, <?php echo htmlentities($_SESSION['user']['username'], ENT_QUOTES, 'UTF-8'); ?></h1>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <a href="logout.php">Logout</a>
</body>
</html>