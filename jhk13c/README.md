```
LIS5900 - Directed Individual Study

 Jee Kim
```
![Jee Pic](./img/Jee1.jpg "Jee")
### ^Jee Kim 


![Screenshot of Powerline Application](./img/PLScreenshot.jpg "PLscreenshot")
*Screenshot of Powerlin application*
[Powerline Link](https://codesolve-eval.azurewebsites.net/FormTmpl/FormtmplView/ "Powerline Link")

### Internship Repo
Link to Jee's Internship Repo:
[Jee's Internship Repo](./internship/README.md "Jee's Internship Repo")

### Login Demo
Link to Login Demo Repo:
[Jee's Login Demo Repo](./login/README.md "Jee's Login Demo Repo")

### Teammate
Stephen Keating:
[Stephen’s Repo](../smk16f/README.md "Stephen's Repo")


### Finished Tasks

* Subdirectory has been created in git
* `README.md` file has been created 
* Subdirectory for "Internship" has been created
* `README.md` file in the internship subdirectory has been created
* Link to the internship subdirectory `README.md` file has been added to the main `README.md` file


### Issues

* Log out feature does not seem to clear browser session ID properly, leaving a user signed in even after signing out
* Received "Bad Request" after logging in


### Benefits

* A great way for people to create an application without knowing in-depth knowlege in database or hiring developers


### Overall Review

* Functionality and usability need a lot of improvement, but this would be a great application for many people/organization/businesses once it improves


### License

For more information about the license, see http://example.com/license/

Copyright (c) 2017 lis4905fall17

Licensed under the Git 'R Dun license.