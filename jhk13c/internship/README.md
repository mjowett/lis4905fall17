```
LIS5900 - Directed Individual Study

 Jee Kim
```


### Internship Application


### Assigned Table => Employer


### Finished Tasks

* Tested "Employer" table with both Valid/Invalid data to confirm the functionality of data validation


### Minor Issues

* inconsistency in the table name. Excel spreadsheet and website display "Employer" table as "Employers". This could cause confusion
* Employer description only allows certain special characters, and apostrophe and @ are not included
* Employer street does not allow underscore and the warning says it allows it
* Employer state only allows capital letters (it would be more convenient for users to be able to use lower case also)
* Employer state currently allows any combination of 2 capital letters
* Employer description section can be resized by clicking and dragging the lower-right hand corner. When the text box gets too big, the web page no longer scale properly


### Bigger Issues

* Employer with same exact information can be added more than once


### Comments

* Are the longitude and latitude information necessary when there are street, city, state, ZIP information already? (perhaps for employer outside of USA?)
* Employer table currently does not allow special character in an employer's name. It's not uncommon to see a special character in a company's name. Any thoughts?
* I suggest adding apostrophe and @ in the regex. Employer description may include additional contact information, and apostrophe could reduce inconvenience



### License

For more information about the license, see http://example.com/license/

Copyright (c) 2017 lis4905fall17

Licensed under the Git 'R Dun license.