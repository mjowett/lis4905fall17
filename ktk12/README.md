# **LIS4905 Directed Independent Study**

## Kyle Kane

**Team mate:** 
- [Andrew Whitely](../anw14b/README.md "Andrew's repo") anw14b@my.fsu.edu

- [Internship subdirectory](internship/README.md)

- [Login Demo](login_demo/README.md)

**Finished tasks:** 
	* Branched Bitbucket
	* Added README.md
	* Petstore web application in Powerline

**Powerline Testing**
	* Overall the idea is a good. Like Stephen said, It is the wordpress for web applications. 
	* The underlying problem right now is the implementation of software. It needs to be more intuitive like Wordpress.

**Benefits:**
	* Powerline can be a tool that anyone can build a web application with
	* Even though you still need prior dev experience to navigate Powerline it is a lot less complex for begining users.

**Issues:**
	* The online GUI had some minor bugs e.g. controls reseting, attributes changing data types.
	* Also had some connectivity issues with the online tool.
	* Broad issue of mine is that it feels like relearning a basic web application course with different names.

**Screenshots**
*Screenshot of Powerline issue*:

![Screen1](/ktk12/img/screen1.png)

*Screenshot of Powerline issue*:

![Screen1](/ktk12/img/screen2.png)