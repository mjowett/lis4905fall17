# **LIS4905 Directed Independent Study**

## Kyle Kane

**Team mate:** 
- [Andrew Whitely](../anw14b/README.md "Andrew's repo")

- [Login Demo](login_demo/README.md)

**Issues:**
	* Last name field does not allow for hypens (regex issue)
	* I don't know if this is a huge problem, but you can submit the same email for email 1 and 2
	* On the edit page you can add over 10 numbers to phone. It gives you an error, but should be like the other phone field

**Suggestions:** 
	*

	