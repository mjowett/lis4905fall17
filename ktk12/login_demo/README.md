# **LIS4905 Directed Independent Study**

## Kyle Kane

**Team mate:** 
- [Andrew Whitely](../anw14b/README.md "Andrew's repo") anw14b@my.fsu.edu

**Login Plan:**
* Username/PW logic:
*	To create a login page first need to add login table to our internship database. 
* Then you could connect and pass the login information into this table using the PDO class. This login page would include data validation to not 	allow users to enter a username that is already taken. 	
* We would add any client-side validation for the password if you need 	to limit to specific characters, length, case sensitivity, etc. 
* I figured we start off by simple user registration and then upgrade to actual FSU student credentials.
- [Reference](http://untame.net/2013/06/how-to-build-a-functional-login-form-with-php-twitter-bootstrap/)
* Bootstrap Formatting:
*The Bootstrap formatting would be simple form with Username and Password fields with a login button. There would be a 	register/sign up that takes in First name, Last name, email, username, and password.
- [Reference](https://www.w3schools.com/bootstrap/bootstrap_forms.asp)

**Login Demo:**
* My website went down, but I can do a demo locally with web tour.

**Session Questions:**
* What is "Session Hijacking"?
	* When someone expoits a valid session to gain unauthorized access to information.
* How can it be prevented?
	* You can encrypt data that is passed by using SSL. This prevents hijackers from seeing the session passed over the internet.
* What would *you* do? Why? Why not?
	* I believe I would encrypt the data by using an SSL. This seems like the most efficient way. The easiest way to hijack a session is by observing traffic passed over internet and exploiting weaknesses. Using an SSL you can encrypt that and thwart hijackers from the easiest way of session hijacking.


	