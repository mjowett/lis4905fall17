# **LIS4905 Directed Independent Study**

## **Andrew Whitely - Fall 17:**

**Team mate:**
- [Kyle Kane:](../ktk12/README.md "Kyle's repo") ktk12@my.fsu.edu

- [Internship subdirectory:](internship/README.md)

- [Login subdirectory:](login/README.md)

- [Login subdirectory:](login/login_demo)

**Finished Tasks**
- Created personal subdirectory and README.md file
- Created tables in powerline web app

*Screenshot of PowerLine :
![Powerline Web App](img/powerline.png)

#### Issues
* Not very responsive, seems clunky when being used

### Benefits
* Although clunky, it is made for those who may not know how to code it themselves
* It is a faster method than doing it all by hand

### Overview
* Not a huge fan, but I can see how it can benefit people.
