# **LIS4905 Directed Independent Study**

## **Andrew Whitely - Fall 17:**

**Team mate:**
- [Kyle Kane:](../../ktk12/README.md "Kyle's repo") ktk12@my.fsu.edu

# Task
- Create a login for the internship web app at http://internship.qcitr.com

# Resources
- [Bootstrap](https://getbootstrap.com)
- [Tutorial Republic](https://www.tutorialrepublic.com/php-tutorial/php-mysql-login-system.php)
- [Github](https://github.com/fethica/PHP-Login)
- [Untame](http://untame.net/2013/06/how-to-build-a-functional-login-form-with-php-twitter-bootstrap/)
- [index.php](index.php)
- PHP
- MySQL

# Idea
- Connect to internship database to test for a valid username/password, if correct then allow user to enter the system and update information/look for internships.
- Bootstrap can be used to create the form, PHP to submit data to MySQL database.