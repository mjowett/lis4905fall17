# **LIS4905 Directed Independent Study**

## **Andrew Whitely - Fall 17:**

## Assigned table: Interns

**Team mate:**
- [Kyle Kane:](../ktk12/README.md "Kyle's repo") ktk12@my.fsu.edu

**Internship application:**
- [Internship Application:](http://internship.qcitr.com)

### Accomplished
- Tested valid, and invalid data
![Valid data](img/valid.png)

![Invalid data](img/invalid.png)

### Issues
- First name and last name field do not allow for hyphens in name
- Formatting and display of table on mobile devices (Side scrolling)
- People can input the same phone number and email as someone else

### Suggestions
- Filter for search field, rather than a general search?
- Way to differentiate seemingly identical students (other than int_id)?