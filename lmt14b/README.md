# **LIS4905 Directed Independent Study**

## **Landon Thaler**

**Team mate:**
- [Jonathan Sapp:](../jss14e/README.md "Jonathan's repo") jss14e@my.fsu.edu

**Login Repo**
- [Login Repo](login/README.md "Login Demo Repo")

**Finished Tasks**
- Created this README.md file
- Created Powerline application

### *Screenshots of Powerline application*:
![Powerline application](img/powerline.PNG)
![Powerline application](img/powerline2.PNG)


## Benefits
* Enter data with ease
* Simple UI to understand
* Great for team collaboration

## Issues
* Website is not responsive
	Some functionality will be lost if you change the browser size
* Design could look better
* Not all buttons support their function
	* Logout request didn't seem to logout fully