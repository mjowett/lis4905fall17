# LIS4905 Directed Independent Study

## Login Repo

### Team:
Jonathan Sapp

Landon Thaler

* Researching what is neccessary for a login function:
    * [research](https://davidwalsh.name/5-features-login-system "Some info acquired from here")
        * "Remember Me" function
            * 92% of users will leave site instead of recovering login info (poll by Blue Research):
                ![poll](poll.pdf "Blue Research Poll")
        * Easy to find Login box
        * "Forgot Your Password" function
    * 
* Bootstrap will be used for easy implementation
    * [example](https://bootsnipp.com/snippets/featured/clean-modal-login-form "of what the login could look like")

* Demo: [Login](http://landonthaler.com/ "Demo")

### Final Questions:
1. What is Session Hijacking?
    * Session hijacking is a method of taking over a Web user session by surreptitiously obtaining the session ID and masquerading as the authorized user. 
2. How can Session Hijacking be prevented?
    * This can be prevented by encryption of the data traffic passed between the parties by using SSL.
3.  What would you do? Why? Why not?
    * In order to prevent this from ever happening *hopefully*, I would get an SSL for my site to stop possible session hijacking. I would then search for a culprit by taking the user and session information from the login page to track a particular user.
