# LIS4905 Directed Independent Study

## Stephen Keating - Internship

### Team mate: - [Jee Kim:](../jhk13c/README.md "Jee's repo") jhk13c@my.fsu.edu

#### Internship Web App: - [Internship Web App](http://internship.qcitr.com/)

**Assigned Table - Employer**

**Finished Tasks**

* Tested "Employer" table with both valid and invalid data. 

**Issues**

* Description allows users to continue entering characters after 1000 character limit has been reached.
* Description does not allow use of exclamation points, question marks, percentage signs, braces, and dollars signs. Might prove to be an issue if taking descriptions off employer websites.
* Employer name field, street, and notes do not tell users the charactor limit for the fields.(45, 30, and 255 respectively)

*Screenshot of issue with description field*:
![Special characters might want to be considered.](img/issue1.PNG)