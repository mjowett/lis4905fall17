# LIS4905 Directed Independent Study

## Stephen Keating

### Team mate:
- [Jee Kim:](../jhk13c/README.md "Jee's repo") jhk13c@my.fsu.edu

### Internship Repo:
Link to Steve's Internship Repo:
[Steve's Internship Repo](internship/README.md "Steve's Internship Repo")

### Login Demo Repo:
Link to Login Demo repo:
[Login Repo](loginDemo"Login Demo Repo")

### Login Demo Zip File
Link ot login demo zip file location:
[Login zip](loginDemo/loginFinal.zip "Login Demo Zip File")

#### Finished Tasks
* Created this README.md file.
* Created powerline app using excel spread sheet.

**Benefits**

* Creates a portable platform to consult with businesses on the go.
* Technicians can create working prototype applications after discussing business rules and requirements with customers.
* Could end up being the "Wordpress" of data driven application development, so long as user interface issues are corrected. 

**Issues**

* Application graphical user interface (GUI) is not intuitive, forcing me to create my pages through the Excel spreadsheet option, then importing them. 
* Using the GUI to create Forms, the table with all the fields runs off the side of the screen.
* I could not set foriegn key values in the GUI, but could upload them from a spreadsheet.
* In the DB Dropdown section of the application, DDL property 1 did not have any values unless I imported the application from a spreadsheet.
* I cannot figure out how to upload data to the application.


*Screenshot of issue with forms fields*:
![Issue with forms fields running over the edge of the screen.](img/forms_field_issue1.PNG)
