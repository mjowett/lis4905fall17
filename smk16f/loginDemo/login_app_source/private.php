 <?php
    require("connection.php");
    if(empty($_SESSION['user']))
    {
        header("Location: index.php");
        die("Redirecting to index.php");
    }
?>


<html>
<head>

<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Login demo for LIS4905">
	<meta name="author" content="Jee Kim and Stephen Keating">
	<link rel="icon" href="favicon.ico">

	<title>LIS4905 Login Demo</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

<!--original style sheet --> 
    <link rel="stylesheet" type="text/css" href="style.css">
    <div class="container-fluid">
		<?php include_once("global/header.php"); ?>	
			</div>

</head>
<body>
    <div>
    <center><h3>Welcome, <?php echo htmlentities($_SESSION['user']['username'], ENT_QUOTES, 'UTF-8'); ?></h3></center>
    <center><div> 
        <?php
        $url = 'http://feeds.bbci.co.uk/news/world/rss.xml';
        $feed = simplexml_load_file($url, 'SimpleXMLIterator');
        echo "<h2>".$feed->channel->description."</h2><ol>";

        $filtered = new LimitIterator($feed->channel->item, 0, 10);
        foreach($filtered as $item)
        {
            ?> 
            <h4><il><a href="<?= $item->link; ?>" target="_blank"><?= $item->title;?></a></il></h4> 
            <?php 
            date_default_timezone_set('America/New_York');

            $date = new DateTime($item->pubDate);
            $date->setTimezone(new DateTimeZone('America/New_York'));
            $offset = $date->getOffset();

            echo $date->format('M j, Y, g:ia').$timezone;
            ?> 
            <p><?php echo $item->description;?></p> 
        <?php } ?> 
        </ol>
        </div></center>
    </div>
<br>
<br> 
    <div>
   <center><button type="button"<a href="logout.php">Logout</a></center>
    </div>
    <br> 

<center><?php include_once "global/footer.php";?></center>
</body>
</html>