<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Login demo for LIS4905">
<meta name="author" content="Jee Kim and Stephen Keating">
<link rel="icon" href="favicon.ico">

<title>LIS4905 Login Demo</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!--original style sheet -->
    <link rel="stylesheet" type="text/css" href="style.css">

    <div class="container-fluid">
		<?php include_once("global/header.php"); ?>	
			</div>
</head>
<body>
<div class="login-page">
  <div class="form">
    <form class="login-form" action="register_process.php" method="post">
      <input type="text" name="username" placeholder="Enter Username"/>
      <input type="password" name="password" placeholder="Enter Password"/>
      <input type="password" name="confirmpass" placeholder="Confirm Password"/> 
      <button>create</button>
      <p class="message">Already registered? <a href="index.php">Sign In</a></p>
    </form>
    	</div>
    </div>

    <center><?php include_once "global/footer.php";?></center>
</body>
</html>