# LIS4905 Directed Independent Study

## Login Demo Repo

### Team:
Jee Kim

Stephen Keating

Link to Jee's Login plan:
[Jee's login plan](../../jhk13c/login "Jee's login plan")

- Researching a basic secure login using PHP and MySQL. 

    - http://forums.devshed.com/php-faqs-stickies-167/program-basic-secure-login-system-using-php-mysql-891201.html

        * step by step breakdown of how to implement
        * thoroughly outlines
            * security issues solved (ie- SQL injection, XSS attacks, password hashing and storage)
            * database interactions (identical to the PDO useage in mobile applications class and CRUD capabilities needed to manage user accounts)
            * general PHP topics needed to create application.
            
        
    - https://github.com/DeveloperShed/BasicSecureLogin
        
        * source code and updates for needed PHP documents to implement system.

<<<<<<< HEAD
- Concept demonstration - ec2-54-164-72-21.compute-1.amazonaws.com/index.php
=======
- Concept demonstration - ec2-54-164-72-21.compute-1.amazonaws.com/index.php

-------------------------------------------------

Link to Login Demo code:
[Login code](login_app_source/README.md "Login Demo Repo")

#### Final Questions
1. What is "Seesion Hacking"?

The exploitation of a valid computer session to gain unauthorized access to information or services in a computer system.

2. How can it be prevented?

You can prevent this by using HTTPS to protect your site. SSL can protect the entire site.
You can also continue to refresh user cookies so that the values change every so often, like 
a required password update every couple months. Also set a time limit to the validity of the 
login with the session cookie used and bam! problems mitigated. 

3. What would you do? Why? Why not?

If i had user information that I needed to protect, I would get an SSL certificate for my site to prevent 
session hijacking. I would also require seesion variables to be set to reauthenticate user login information 
every time they accessed the site to make sure session variables could only be used once. 

-------------------------------------------------

4. The Primary Key Debate 

In any situation where I am protecting sesnitive information or user data, I would use a GUID. It seems to be the most 
secure option of the two, and eliminates the "guesswork" option some system hackers could take with URLs. It would also
help compartmentalize any important information sent to users via email and protect emails as well. 

If performance was a concern, using a hybrid system would be my choice. Something along the lines of Twitter's Snowflake project,
just a more recent and not shutdown version of it. The only time I would use integer PKs again is for a personal project that I
was not worried about securing.
>>>>>>> smk16f
