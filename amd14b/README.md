# **LIS4905 Directed Independent Study**

## **Ariana M. Davis - Fall 17:**

**Teammate:**
- [Benjamin Bloodworth:](../bbloodworth/README.md "Benjiman's repo") 

**Powerline Web App:**
- [Powerline Web App](https://codesolve-eval.azurewebsites.net/FormTmpl/FormtmplView) 

**Internship Web App:**
- [Internship Web App](internship/README.md) 

**Login Web App Demo:**
- [Login Web App Demo](internship/login-demo-v1.zip) 

**Completed Tasks**
- Created personal subdirectory and README.md file

*Screenshot of PowerLine Web App forms fields*:
![Powerline WebA App](img/powerline_1.png)

#### Issues
* Wasnt that user - friendly when uploading data to the tables
* We had an issue when setting a foriegn key values in the user interface table 
* While we updated the data to the Excel spreadsheet , there was a problem transferring data over to the Powerline Web App
* The data from the Excel sheet wouldn't transfer over at times when uploading to Powerline

#### Benefits 
* One benefit was being able to create tables in a much quicker process
* Being able to collaborate with your teammate is easier

#### Overall Review 
* I personally wouldn't use Powerline as a go-to resource, there's still a good bit of changes needed to be completed before being released
* Changes in regards to having a bit smoother process when validating data 

## License

For more information about the license, see http://example.com/license/

Copyright (c) 2017 **lis4905fall17**

Licensed under the **Git 'R Dun** license.
