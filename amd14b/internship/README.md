# **LIS4905 Directed Independent Study**

## **Ariana M. Davis - Internship:**

**Teammate:**
- [Benjamin Bloodworth:](../bbloodworth/README.md "Benjiman's repo") 

**Internship Web App:**
- [Internship Web App](http://internship.qcitr.com/) 

**Internship Login Form Example:**
- [Login Form Example:](login-form "Login Box Example") 


### Issues
### Landing Page: 
* Able to retrieve forgien language entires
* Accepts any character when entering information on the internship site
* Does not reflect the most recently entered intern's information
* Not responsive on desktop view, mobile layout seems pretty unstructued 

### Assignments: 
* Testing valid & invalid data in coordination with parent/child team/table
* Seems like a faster method when recongizing certain internships

#### Errors: 
* Some information sometimes disappers when entering information in the corresponding fileds

#### Recommendations: 
* There should be a limit to how many characters can be entered when entering information, mainly helping with character validation

### Internship Web App Login Strategy:
* Purchase an SSL certificate and add it to the server. Login and password information should never be sent across the internet unencrypted. An SSL certificate should be purchased to protect everyone's login information.
* Create a server side redirect rule that redirects an http request for this page to an https request. This way we can enforce a secure connection.
* UI should include fields for login and password.
* UI should include a link to reset a user's password in case they forgot it.
* UI should provide validation feedback to the user if they fail to provide a valid username and password. We don't want to specify which part of the combination is invalid because that is additional information a malicious user can use to try and compromise a login. We just need to display a generic message that their login infomration is incorrect.
* UI should do client side validation before submitting to the server. If we decide to use email addresses for passwords, then the UI should validate that a valid email address is provided for the login field. Additionally, we need to decide on some rules for a valid password. What is the minimum and maximum length of the password? Are we going to require letters, numbers, and special symbols? The UI should validate that the password meets these rules before submitting to the server.
* Server side processing script should perform the same username and password validation before making the database request.
* Service side processing script needs to return an error message to the client in the event that the username or password is invalid. The UI will be responsible for rendering this message.
* To prevent brute force attacks, we should display a CAPTCHA challenge after 3 failed logins. We don't want to lock the account out due to failed logins because that creates a way for a malicious user to lock someone out of their account. A CAPTCHA challenge will stop brute force attacks without preventing a legitimate user from accessing their account. By not requiring a CAPTCHA unless the current user has failed three to login successfully three times in a row, we eliminate frustrations for legitimate users.
* It would be nice to integrate the login with the FSU domain server, but it's unclear if this application allows individuals without an FSU account to access the application.
* If we can't integrate with the FSU domain server, then we need to develop a database backend for storing login information. We need to add a salting and encryption algorithim to secure the user's password in the database table. 

## License

For more information about the license, see http://example.com/license/

Copyright (c) 2017 **lis4905fall17**

Licensed under the **Git 'R Dun** license.